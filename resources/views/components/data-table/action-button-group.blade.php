<div class="btn-group">
    <button type="button" class="btn btn-sm btn-success" onclick="handleEditClick({{ $dataKey }}, this)">
        <i class="fa fa-pen" aria-hidden="true"></i> Edit
    </button>
    <button type="button" class="btn btn-sm btn-danger" onclick="handleRemoveClick({{ $dataKey }}, this)">
        <i class="fa fa-trash" aria-hidden="true"></i> Remove
    </button>
</div>
