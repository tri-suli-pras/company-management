<x-app contentHeader="Employees">
    <x-slot name="content">
        <div class="card">
            <div class="card-header">
                <div class="row justify-content-between">
                    <div class="col-4">
                      <h3 class="card-title">List of all employees</h3>
                    </div>
                    <div class="col-2">
                        <button type="button" class="btn btn-info btn-block btn-flat" data-toggle="modal" data-target="#modalForm">
                            <i class="fa fa-plus"></i> Create New
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form class="form-inline">
                    @csrf
                    <div class="form-group mx-sm-3 mb-2">
                        <input id="fn" name="first_name" type="text" class="form-control" placeholder="First Name">
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <input id="ln" name="last_name" type="text" class="form-control" placeholder="Last Name">
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <input id="ma" name="email" type="email" class="form-control" placeholder="Email">
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <input id="co" name="company" type="text" class="form-control" placeholder="Company">
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <input id="fr" name="from" type="date" class="form-control" placeholder="From">
                    </div>
                    <div class="form-group mx-sm-3 mb-2">
                        <input id="to" name="to" type="date" class="form-control" placeholder="To">
                    </div>
                    <button id="searchEmployee" type="button" class="btn btn-primary mb-2">Search</button>
                </form>
                <table id="employee-table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Index</th>
                            <th>Full Name</th>
                            <th>Company</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Added</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="modal fade" id="modalForm" style="display: none;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Employe Form</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card">
                            <div class="card-body">
                                <form name="employee" action="{{ route('employees.store') }}" method="POST">
                                    @csrf
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <label class="col-form-label" for="inputError">
                                                @if ($errors->has('first_name'))
                                                    <i class="far fa-times-circle"></i>                                
                                                @endif First Name
                                            </label>
                                            <input
                                                type="text"
                                                name="first_name"
                                                value="{{ old('first_name') }}"
                                                aria-describedby="first_name"
                                                class="form-control {{ $errors->has('first_name') ? 'is-invalid' : '' }}"
                                            />
                                            @if ($errors->has('first_name'))
                                                @foreach ($errors->get('first_name') as $message)
                                                    <small id="first_name" class="form-text text-muted">{{ $message }}</small>
                                                @endforeach           
                                            @endif
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="col-form-label" for="inputError">
                                                @if ($errors->has('last_name'))
                                                    <i class="far fa-times-circle"></i>                                
                                                @endif Last Name
                                            </label>
                                            <input
                                                type="text"
                                                name="last_name"
                                                value="{{ old('last_name') }}"
                                                aria-describedby="last_name"
                                                class="form-control {{ $errors->has('last_name') ? 'is-invalid' : '' }}"
                                            />
                                            @if ($errors->has('last_name'))
                                                @foreach ($errors->get('last_name') as $message)
                                                    <small id="last_name" class="form-text text-muted">{{ $message }}</small>
                                                @endforeach           
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" for="inputError">
                                            @if ($errors->has('company_id'))
                                                <i class="far fa-times-circle"></i>                                
                                            @endif Company
                                        </label>
                                        <select
                                            name="company_id"
                                            aria-describedby="company"
                                            class="form-control {{ $errors->has('company_id') ? 'is-invalid' : '' }}"
                                        >
                                            <option value="null" selected disabled>Choose company</option>
                                            @foreach ($companies as $company)
                                                <option value="{{ $company->id }}">{{ $company->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('company_id'))
                                            @foreach ($errors->get('company_id') as $message)
                                                <small id="company_id" class="form-text text-muted">{{ $message }}</small>
                                            @endforeach           
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" for="inputError">
                                            @if ($errors->has('email'))
                                                <i class="far fa-times-circle"></i>                                
                                            @endif Email
                                        </label>
                                        <input
                                            name="email"
                                            type="email"
                                            value="{{ old('email') }}"
                                            aria-describedby="email"
                                            class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                                        />
                                        @if ($errors->has('email'))
                                            @foreach ($errors->get('email') as $message)
                                                <small id="email" class="form-text text-muted">{{ $message }}</small>
                                            @endforeach           
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" for="inputError">
                                            @if ($errors->has('phone'))
                                                <i class="far fa-times-circle"></i>                                
                                            @endif Phone
                                        </label>
                                        <input
                                            type="text"
                                            name="phone"
                                            value="{{ old('phone') }}"
                                            aria-describedby="phone"
                                            class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}"
                                        />
                                        @if ($errors->has('phone'))
                                            @foreach ($errors->get('phone') as $message)
                                                <small id="phone" class="form-text text-muted">{{ $message }}</small>
                                            @endforeach           
                                        @endif
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button data-form="employee" type="button" class="btn btn-info">Submit</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalCompany" style="display: none;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Company Detail</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="companyCard" class="card card-outline card-dark">
                            <div class="card-header">
                                <h3 class="card-title">Default Card Example</h3>
                                <div class="card-tools">
                                    <!-- Buttons, labels, and many other things can be placed here! -->
                                    <!-- Here is a label for example -->
                                    <span class="badge badge-default"></span>
                                </div>
                            </div>
                            <div class="card-body">
                                <img width="100%" src="" alt="undefined" />
                            </div>
                            <div class="card-footer">
                                The footer of the card
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
    <x-slot name="styles">
        
    </x-slot>
    <x-slot name="scripts">
        @section('plugins.Datatables', true)
        <script>
            const message = "{{ session('message') }}"

            if (message) {
                Swal.fire(message)
            }

        </script>
        <script>
            const hasErrors = '{{ $errors->any() }}'
            const modalForm = $('#modalForm');
            const form = $('form[name=employee]');

            if (Boolean(hasErrors)) {
                modalForm.modal().show();
            }

            function handleEditClick (subject, btn) {
                const btnPen = $(btn);
                const td = btnPen.closest('td');

                td.prevAll().each(function (index, item) {
                    const td = $(item);

                    switch (index) {
                        case 0:
                            $('input[name=phone]').val(td.text());
                            break;
                        case 1:
                            $('input[name=email]').val(td.text());
                            break;
                        case 2:
                            const company = td.find('a').text();
                            $('select[name=company_id] option').filter(function () {
                                return $(this).html() === company;
                            }).attr('selected', true);
                            break;
                        case 3:
                            $('input[name=first_name]').val($('#fn').text())
                            $('input[name=last_name]').val($('#ln').text())
                    }
                })

                form.attr('action', `{{ url("employees") }}/${subject}`)
                $('<input type="hidden" name="_method" value="PUT">').insertAfter(
                    $('input[name=_token]')
                )

                modalForm.modal().show();
            }

            function handleRemoveClick (subject) {
                Swal.fire({
                    title: 'Are you sure want to delete this record?',
                    showCancelButton: true,
                    confirmButtonText: `Delete`,
                    confirmButtonColor: '#dc3545',
                    footer: '<h6 style="color: red;">Data will be permanently deleted!</h6>',
                }).then(function (result) {
                    if (result.value) {
                        form.attr('action', `{{ url("employees") }}/${subject}`)
                        $('<input type="hidden" name="_method" value="DELETE">').insertAfter(
                            $('input[name=_token]')
                        );
                        form.submit();
                    }
                })
            }

            function handleShowCompany (company) {
                const modalCompany = $('#modalCompany');
                const card = $('#companyCard');

                card.find('.card-title').text(company.name);
                card.find('.card-tools span').text(company.email);
                card.find('.card-body img').attr('src', company.logo);
                card.find('.card-footer').text(company.website);

                modalCompany.modal().show();
            }

            $(function () {
                const table = $("#employee-table").DataTable({
                    responsive: true, 
                    autoWidth: true,
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{ url('api/employees/paginate') }}",
                        data: function (data) {
                            data.first_name = $('#fn').val();
                            data.last_name = $('#ln').val();
                            data.email = $('#ma').val();
                            data.company = $('#co').val();
                            data.from = $('#fr').val();
                            data.to = $('#to').val();
                        }
                    },
                    columns: [
                        { data: 'DT_RowIndex', orderable: false, searchable: false },
                        { data: 'fullname' },
                        { data: 'company', },
                        { data: 'email' },
                        { data: 'phone' },
                        { data: 'added', searchable: false },
                        { data: 'action', orderable: false, searchable: false }
                    ]	
                });

                $('#searchEmployee').click(function (e) {
                    table.draw();
                    e.preventDefault();
                })
            });
        </script>
        <script>
            $(function () {
                const button = $('button[data-form=employee]');

                button.click(function (e) {
                    form.submit();
                    $modalForm.modal('hide');
                });

                modalForm.on('hidden.bs.modal', function (e) {
                    form.attr('action', "{{ route('employees.store') }}")
                    form.find('input[name=_method]').remove()
                    form.trigger('reset')
                });
            })
        </script>
    </x-slot>
</x-app>
