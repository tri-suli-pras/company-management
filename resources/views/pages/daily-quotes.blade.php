<x-app contentHeader="Daily Quotes">
    <x-slot name="content">
        <div class="row">
            <div class="col">
                <button type="button" class="btn btn-primary btn-block" onclick="location.reload();">
                    <i class="fa fa-load"></i> Refresh Quotes
                </button>
            </div>
        </div>
        <div class="row">
            @if (is_array($quotes))
                @foreach ($quotes as $quote)
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">
                                <i class="fas fa-text-width"></i>
                                {{ $quote->q }}
                            </h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            {!! $quote->h !!}
                            <small>{{ $quote->a }}</small>
                        </div>
                    </div>
                </div>
                @endforeach
            @endif
        </div>
    </x-slot>
    <x-slot name="styles">
        
    </x-slot>
    <x-slot name="scripts">
    </x-slot>
</x-app>
