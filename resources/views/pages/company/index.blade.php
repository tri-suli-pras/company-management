<x-app contentHeader="Companies">
    <x-slot name="content">
        <div class="card">
            <div class="card-header">
                <div class="row justify-content-between">
                    <div class="col-4">
                      <h3 class="card-title">List of all companies</h3>
                    </div>
                    <div class="col-2">
                        <button type="button" class="btn btn-info btn-block btn-flat" data-toggle="modal" data-target="#companyModalForm">
                            <i class="fa fa-plus"></i> Create New
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table id="company-table" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th width="1%">Index</th>
                            <th width="50%">Name</th>
                            <th width="10%">Email</th>
                            <th width="10%">Logo</th>
                            <th width="10%">Website</th>
                            <th width="18%">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="modal fade" id="companyModalForm" style="display: none;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Company Form</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card">
                            <div class="card-body">
                                <form name="company" enctype="multipart/form-data" action="{{ route('companies.store') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label class="col-form-label" for="inputError">
                                            @if ($errors->has('name'))
                                                <i class="far fa-times-circle"></i>                                
                                            @endif Name
                                        </label>
                                        <input
                                            type="text"
                                            name="name"
                                            value="{{ old('name') }}"
                                            aria-describedby="name"
                                            class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                        />
                                        @if ($errors->has('name'))
                                            @foreach ($errors->get('name') as $message)
                                                <small id="name" class="form-text text-muted">{{ $message }}</small>
                                            @endforeach           
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" for="inputError">
                                            @if ($errors->has('email'))
                                                <i class="far fa-times-circle"></i>                                
                                            @endif Email
                                        </label>
                                        <input
                                            name="email"
                                            type="email"
                                            value="{{ old('email') }}"
                                            aria-describedby="email"
                                            class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                                        />
                                        @if ($errors->has('email'))
                                            @foreach ($errors->get('email') as $message)
                                                <small id="email" class="form-text text-muted">{{ $message }}</small>
                                            @endforeach           
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" for="inputError">
                                            @if ($errors->has('website'))
                                                <i class="far fa-times-circle"></i>                                
                                            @endif Website
                                        </label>
                                        <input
                                            type="text"
                                            name="website"
                                            value="{{ old('website') }}"
                                            aria-describedby="website"
                                            class="form-control {{ $errors->has('website') ? 'is-invalid' : '' }}"
                                        />
                                        @if ($errors->has('website'))
                                            @foreach ($errors->get('website') as $message)
                                                <small id="website" class="form-text text-muted">{{ $message }}</small>
                                            @endforeach           
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" for="inputError">
                                            @if ($errors->has('logo'))
                                                <i class="far fa-times-circle"></i>                                
                                            @endif Logo
                                        </label>
                                        <input
                                            name="logo"
                                            type="file"
                                            aria-describedby="logo"
                                            class="form-control {{ $errors->has('logo') ? 'is-invalid' : '' }}"
                                        />
                                        @if ($errors->has('logo'))
                                            @foreach ($errors->get('logo') as $message)
                                                <small id="logo" class="form-text text-muted">{{ $message }}</small>
                                            @endforeach           
                                        @endif
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button data-form="company" type="button" class="btn btn-info">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>
    <x-slot name="styles">
        
    </x-slot>
    <x-slot name="scripts">
        @section('plugins.Datatables', true)
        <script>
            const message = "{{ session('message') }}"

            if (message) {
                Swal.fire(message)
            }

        </script>
        <script>
            const hasErrors = '{{ $errors->any() }}'
            const modalForm = $('#companyModalForm');
            const form = $('form[name=company]');

            if (Boolean(hasErrors)) {
                modalForm.modal().show();
            }

            function handleEditClick (subject, btn) {
                const btnPen = $(btn);
                const td = btnPen.closest('td');

                td.prevAll().each(function (index, item) {
                    if (index <= 1) {
                        const link = $(item).find('a').attr('href');
                        if (link) {
                            $('input[name=website]').val(link)
                        }
                    } else if (index === 2) {
                        $('input[name=email]').val(
                            $(item).text()
                        )
                    } else if (index === 3) {
                        $('input[name=name]').val(
                            $(item).text()
                        )
                    }
                })

                form.attr('action', `{{ url("companies") }}/${subject}`)
                $('<input type="hidden" name="_method" value="PUT">').insertAfter(
                    $('input[name=_token]')
                )

                modalForm.modal().show();
            }

            function handleRemoveClick (subject) {
                Swal.fire({
                    title: 'Are you sure want to delete this record?',
                    showCancelButton: true,
                    confirmButtonText: `Delete`,
                    confirmButtonColor: '#dc3545',
                    footer: '<h6 style="color: red;">Data will be permanently deleted!</h6>',
                }).then(function (result) {
                    if (result.value) {
                        form.attr('action', `{{ url("companies") }}/${subject}`)
                        $('<input type="hidden" name="_method" value="DELETE">').insertAfter(
                            $('input[name=_token]')
                        );
                        form.submit();
                    }
                })
            }

            $(function () {
                const table = $("#company-table").DataTable({
                    responsive: true, 
                    autoWidth: false,
                    processing: true,
                    serverSide: true,
                    ajax: "{{ url('api/companies/paginate') }}",
                    columns: [
                        { data: 'DT_RowIndex', orderable: false, searchable: false },
                        { data: 'name' },
                        { data: 'email' },
                        { data: 'logo' },
                        { data: 'website' },
                        { data: 'action' }
                    ]	
                });
            });
        </script>
        <script>
            $(function () {
                const button = $('button[data-form=company]');

                button.click(function (e) {
                    form.submit();
                    $modalForm.modal('hide');
                });

                modalForm.on('hidden.bs.modal', function (e) {
                    form.attr('action', "{{ route('companies.store') }}")
                    form.find('input[name=_method]').remove()
                    form.trigger('reset')
                });
            })
        </script>
    </x-slot>
</x-app>
