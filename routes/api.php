<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\CompanyPaginationController;
use App\Http\Controllers\Api\EmployeePaginationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('companies/paginate', CompanyPaginationController::class);
Route::get('employees/paginate', EmployeePaginationController::class);
