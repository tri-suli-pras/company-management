## About App

Application management for manage comanies and employees. This application is build using [Laravel](https://laravel.com/) framework.
The dashboard is using [AdminLTE](https://github.com/jeroennoten/Laravel-AdminLTE).

- Manage company.
- Manage employee.
- Send notification to company if new employee was added.

## Framework

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## App Installation

- Clone this repository && cd into it
- Run `composer install`
- Run `cp .env.example .env`
- Setup your database by updating `.env`
- Run `php artisan key:generate`
- Run  `php artisan optimize`
- Run `npm install && npm run dev`

## Admin LTE Installation

- Run `php artisan adminlte:install`
- Run `php artisan adminlte:install --only=auth_views`
