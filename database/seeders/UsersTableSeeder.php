<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $domainEmail = config('company.email_domain');

        User::factory()->createMany([
            ['email' => "admin@$domainEmail", 'role' => User::ROLE_ADMIN],
            ['email' => "user@$domainEmail", 'role' => User::ROLE_USER],
        ]);
    }
}
