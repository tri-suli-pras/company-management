<?php

namespace App\View\Components;

use Illuminate\View\Component;

class App extends Component
{
    /**
     * Describe the content header of the page
     *
     * @var string
     */
    public $contentHeader;

    /**
     * Create a new component instance.
     *
     * @param string $contentHeader
     * @return void
     */
    public function __construct(string $contentHeader = "Dashboard")
    {
        $this->contentHeader = $contentHeader;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.app');
    }
}
