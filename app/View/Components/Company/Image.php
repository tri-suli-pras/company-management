<?php

namespace App\View\Components\Company;

use Illuminate\View\Component;

class Image extends Component
{
    /**
     * Describe the image source
     *
     * @var string
     */
    public $src;

    /**
     * Image alternative text to display
     *
     * @var string
     */
    public $alt;

    /**
     * Create a new component instance.
     *
     * @param string $src
     * @param string $alt
     * @return void
     */
    public function __construct(string $src, string $alt = 'undefined')
    {
        $this->src = $src;
        $this->alt = $alt;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.company.image', [
            'src' => $this->src,
            'alt' => $this->alt
        ]);
    }
}
