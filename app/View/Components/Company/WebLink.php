<?php

namespace App\View\Components\Company;

use Illuminate\Support\Str;
use Illuminate\View\Component;

class WebLink extends Component
{
    /**
     * Text that represent the link
     *
     * @var string
     */
    public $displayText;

    /**
     * Href link
     *
     * @var string
     */
    public $link;

    /**
     * Create a new component instance.
     *
     * @param string $link
     * @param string|null $displayText
     * @return void
     */
    public function __construct(string $link, string $displayText = null)
    {
        $this->link = $link;

        if (is_null($displayText)) {
            $url = parse_url($link);
            $this->displayText = $url['host'];
        } else {
            $this->displayText = Str::title($displayText);
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.company.web-link', [
            'link' => $this->link,
            'displayText' => $this->displayText 
        ]);
    }
}
