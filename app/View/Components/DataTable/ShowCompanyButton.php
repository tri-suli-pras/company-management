<?php

namespace App\View\Components\DataTable;

use App\Models\Company;
use Illuminate\View\Component;

class ShowCompanyButton extends Component
{
    /**
     * Company instance
     *
     * @var Company
     */
    public $company;

    /**
     * Create a new component instance.
     *
     * @param Company $company
     * @return void
     */
    public function __construct($company)
    {
        $this->company = $company;

        if ($company instanceof Company) {
            $this->company->logo = asset($company->logo);
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.data-table.show-company-button', [
            'company' => $this->company
        ]);
    }
}
