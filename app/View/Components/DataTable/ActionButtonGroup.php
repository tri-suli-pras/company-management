<?php

namespace App\View\Components\DataTable;

use Illuminate\View\Component;

class ActionButtonGroup extends Component
{
    /**
     * The data key reference
     *
     * @var int|string
     */
    public $dataKey;

    /**
     * Create a new component instance.
     *
     * @param int|string $dataKey
     * @return void
     */
    public function __construct($dataKey)
    {
        $this->dataKey = $dataKey;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.data-table.action-button-group', [
            'dataKey' => $this->dataKey
        ]);
    }
}
