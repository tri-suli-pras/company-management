<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Company;
use App\Http\Controllers\Controller;
use App\View\Components\Company\Image;
use App\View\Components\Company\WebLink;
use App\View\Components\DataTable\ActionButtonGroup;

class CompanyPaginationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $companies = Company::select(['id', 'name', 'email', 'logo', 'website'])->orderBy('id');
        
        return DataTables::of($companies)
                ->editColumn('logo', function ($model) {
                    return (new Image($model->logo))->render();
                })
                ->editColumn('website', function ($model) {
                    return (new WebLink($model->website))->render();
                })
                ->addColumn('action', function ($model) {
                    return (new ActionButtonGroup($model->id))->render();
                })
                ->addIndexColumn()
                ->rawColumns(['logo', 'action', 'website'])
                ->toJson();
    }
}
