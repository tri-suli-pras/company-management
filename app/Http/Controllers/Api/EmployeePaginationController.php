<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Employee;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\View\Components\DataTable\ActionButtonGroup;
use App\View\Components\DataTable\ShowCompanyButton;
use Illuminate\Support\Facades\DB;

class EmployeePaginationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $employees = Employee::with('company:id,name,email,logo,website')->select([
            'id',
            'company_id',
            'first_name',
            'last_name',
            'email',
            'phone',
            'created_at'
        ])->orderBy('id');

        if ($request->has('first_name')) {
            $employees->where('first_name', 'like', "%$request->first_name%");
        }

        if ($request->has('last_name')) {
            $employees->where('last_name', 'like', "%$request->last_name%");
        }

        if ($request->has('email')) {
            $employees->where('email', 'like', "%$request->email%");
        }

        if (!is_null($request->from) || !is_null($request->to)) {
            if ($request->from && $request->to) {
                $employees->whereBetween(DB::raw('DATE(created_at)'), [$request->from, $request->to]);
            }
        }

        if ($request->has('company')) {
            $employees->whereHas('company', function ($query) use ($request) {
                $query->where('name', 'like', "%$request->company%");
            });
        }
        
        return DataTables::of($employees)
                ->editColumn('fullname', function ($model) {
                    return $model->fullName . '<span id="fn" hidden>'. $model->first_name .'</span>' . '<span id="ln" hidden>'. $model->last_name .'</span>';
                })
                ->editColumn('added', function ($model) {
                    return $model->created_at->diffForHumans();
                })
                ->editColumn('company', function ($model) {
                    return (new ShowCompanyButton($model->company))->render();
                })
                ->addColumn('action', function ($model) {
                    return (new ActionButtonGroup($model->id))->render();
                })
                ->filterColumn('fullname', function($query, $keyword) {
                    $sql = "CONCAT(employees.first_name,'-',employees.last_name)  like ?";
                    $query->whereRaw($sql, ["%{$keyword}%"]);
                })
                ->filterColumn('company', function($query, $keyword) {
                    $companies = Company::select('id')->where('name', 'like', "%$keyword%")->get();
                    $query->whereIn('company_id', $companies->toArray());
                })
                ->addIndexColumn()
                ->rawColumns(['fullname', 'company', 'action'])
                ->toJson();
    }
}
