<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Models\Company;
use App\Http\Requests\Company\StoreCompanyRequest;
use App\Http\Requests\Company\UpdateCompanyRequest;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.company.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreCompanyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCompanyRequest $request)
    {
        $company = $request->only('name', 'email', 'website');
        
        $company = Company::create($company);
        
        if ($company) {
            $path = $request->logo->store("images/companies/{$company->id}", config('filesystems.default'));
            $company->logo = $path;
            $company->save();
        }

        return redirect()->back()->with('message', 'Company created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCompanyRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompanyRequest $request, $id)
    {
        $company = Company::find($id);
        $attribute = $request->only('name', 'email', 'website');
        
        if ($request->hasFile('logo')) {
            Storage::disk(config('filesystems.default'))->delete($company->logo);
            $path = $request->logo->store("images/companies/{$id}", config('filesystems.default'));
            $attribute['logo'] = $path;
        }

        $company->update($attribute);

        return redirect()->back()->with('message', 'Company updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        
        Storage::disk(config('filesystems.default'))->delete($company->logo);

        $company->delete();

        return redirect()->back()->with('message', 'Company deleted successfully!');
    }
}
